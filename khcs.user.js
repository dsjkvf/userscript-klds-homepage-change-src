// ==UserScript==
// @name        KLDS homepage - change src
// @namespace   oyvey
// @homepage    https://bitbucket.org/dsjkvf/userscript-klds-homepage-change-src/
// @downloadURL https://bitbucket.org/dsjkvf/userscript-klds-homepage-change-src/raw/master/khcs.user.js
// @updateURL   https://bitbucket.org/dsjkvf/userscript-klds-homepage-change-src/raw/master/khcs.user.js
// @version     1.09
// @match       https://infornography.kpoxa.org/personal/s/tmp/klds/*
// @run-at      document-start
// @grant       none
// ==/UserScript==


// INIT

var sources;
if (top.window.location.pathname.split('/')[top.window.location.pathname.split('/').length - 2] != 'klds') {
    sources = ['../reu.cgi', '../wsj.cgi', '../ftm.cgi', '../rbc.cgi', '../fnz.cgi'];
} else {
    sources = ['reu.cgi', 'wsj.cgi', 'ftm.cgi', 'grd.cgi', 'rbc.cgi', 'fnz.cgi'];
};


// HELPERS

// Change background color
function prepareMySalmon() {
    document.body.style.background = '#FCD5B5';
    var nodes = document.getElementsByTagName("*");
    for(var i=0; i<nodes.length; i++) {
        if (nodes[i].nodeName.toLowerCase() == 'div' ||
            nodes[i].nodeName.toLowerCase() == 'header' ||
            nodes[i].nodeName.toLowerCase() == 'p' ||
            nodes[i].nodeName.toLowerCase() == 'td' ||
            nodes[i].nodeName.toLowerCase() == 'tr' ||
            nodes[i].nodeName.toLowerCase() == 'a'
        ) {
            nodes[i].style.background = "#FCD5B5";
        };
    };
}


// MAIN

// Add a hotkey to switch between sources
addEventListener('keyup', function(e) {
    if ( e.ctrlKey ||
         e.altKey ||
         e.metaKey ||
         e.shiftKey ||
         (e.which !== 88 /*x*/ && e.which !== 82 /*r*/ && e.which !== 192 /*`*/ && e.which != 67 /*c*/) ) {
         return;
    };

    e.preventDefault();

    // get current src for the frame with this class
    var curr = top.window.document.getElementsByTagName("frame")[1].src;
    // extract the exact script name
    curr = curr.substr(curr.lastIndexOf('/') + 1);
    var next;
    if (e.which == 88) {
        // get the source, which is next to this in the sources array
        next = sources[sources.findIndex(src => src.indexOf(curr) > -1) + 1];
        // if current was the last one in the array, set the next one to the first
        if (typeof next == "undefined"){
            next = sources[0];
        };
        // change source
        top.window.document.getElementsByTagName("frame")[1].src = next
    } else if (e.which == 192) {
        next = sources[sources.findIndex(src => src.indexOf(curr) > -1) - 1];
        if (typeof next == "undefined"){
            next = sources[sources.length - 1];
        };
        // change source
        top.window.document.getElementsByTagName("frame")[1].src = next
    } else if (e.which == 82) {
        top.window.document.getElementsByTagName("frame")[1].contentDocument.location.reload();
    } else if (e.which == 67) {
        // change color
        prepareMySalmon();
    }
})
